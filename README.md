Implementação do Teste de FizzBuzz de minha autoria, feito em C++.

Observação: Os testes FizzBuzz usuais costumam colocar um loop até um numero fixo. Minha implementação é um pouco modificada e o loop continua infinitamente.